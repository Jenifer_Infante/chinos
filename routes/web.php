<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('paises',function(){

//crear un arreglo con informacion de paises 
$paises=[
    "Colombia" =>[
                  "capital" => "Bogota",
                  "moneda" => "peso",
                  "poblacion" => 50.372
               ],
    "Ecuador" =>[
                 "capital" => "Quito",
                 "moneda" => "Dolar",
                 "poblacion" => 17.517
    ],
    "Chile"=>[
               "capital" => "Santiago de Chile",
                "moneda" => "Real",
                "poblacion" => 203.517
    ],
    "Argentina" =>[
        "capital" => "Buenos Aires",
        "moneda" => "Dolar",
        "poblacion" => 11.633

    ]
];

//Como recorrer la primera diemension del arreglo
/*foreach ($paises as $pais => $infopais){
   echo  "<h2> $pais </h2>";
   echo "capital:". $infopais ["capital"]."<br/>";
   echo "moneda". $infopais["moneda"]."<br/>";
   echo "poblacion(en millones de habitantes )". $infopais["poblacion"]."<br/>";
   echo "<hr/>";

}
*/

//Mostrar una Vista para presentar los paises 
// en MVC yompuedo pasar datos a una vista 
 return view('paises')->with("paises", $paises );

});